sinabs.SynOpCounter
===================

.. automodule:: sinabs.synopcounter
.. py:currentmodule:: sinabs.synopcounter

`SynOpCounter`
~~~~~~~~~~~~~~

.. autoclass:: SynOpCounter
    :members:

